# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala |4 hodin |
| jak se mi to podařilo rozplánovat | dělal jsem to na poslední chvíli |
| design zapojení |https://gitlab.spseplzen.cz/hybesp/4h-vlastni-shield/-/blob/main/dokumentace/fotky/IMG_8306.jpg |
| proč jsem zvolil tento design | takhle se to vešlo na shield |
| zapojení | https://gitlab.spseplzen.cz/hybesp/4h-vlastni-shield/-/blob/main/dokumentace/schema/schema.png |
| z jakých součástí se zapojení skládá | rezistory, termistor, fotorezistor, shield, LEDs|
| realizace |  https://gitlab.spseplzen.cz/hybesp/4h-vlastni-shield/-/blob/main/dokumentace/fotky/IMG_8305.jpg |
| nápad, v jakém produktu vše propojit dohromady| chytrý prvek domácnosti s ovládáním, které má odesílat data na webserver |
| co se mi povedlo | ---- |
| co se mi nepovedlo/příště bych udělal/a jinak | pájení |
| zhodnocení celé tvorby | Příště lépe rozplánovat, a také lépe napájet |
