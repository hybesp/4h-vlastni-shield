#include <OneWire.h>
#include <DallasTemperature.h>
int green = D9;
int red = D8;

// připojení potřebné knihovny
#include <Adafruit_NeoPixel.h>
// nastavení propojovacího pinu
#define pinDIN D10
// nastavení počtu LED modulů
#define pocetLED 8
// inicializace LED modulu z knihovny
Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);


// pin pro teplotního čidla
const int oneWireBus = D6;     

// nastavení onewire instance pro komunikaci s oneWire zařízeními
OneWire oneWire(oneWireBus);
DallasTemperature sensors(&oneWire);

// proměnné pro nastavení propojovacích pinů enkodéru
int pinCLK = D2;
int pinDT  = D3;
int pinSW  = D4;

// proměnné pro uložení pozice a stavů pro určení směru
// a stavu tlačítka
int poziceEnkod = 0;
int stavPred;
int stavCLK;
int stavSW;

//fotorezistor
int photo = A0;


void setup() {
pinMode(photo, INPUT);
Serial.begin(115200);
  // Start the DS18B20 sensor
sensors.begin();
Serial.println("Jedu");

  // nastavení propojovacích pinů jako vstupních
pinMode(pinCLK, INPUT);
pinMode(pinDT, INPUT);
  // nastavení propojovacího pinu pro tlačítko
  // jako vstupní s pull up odporem
pinMode(pinSW, INPUT_PULLUP);
  // načtení aktuálního stavu pinu CLK pro porovnávání
stavPred = digitalRead(pinCLK);   
pinMode(green, OUTPUT);
pinMode(red, OUTPUT);

// zahájení komunikace s LED modulem
  rgbWS.begin();

}

void loop() {
   Photo();
  //Temperature();
  //Encoder();
  RGB();
  digitalWrite(green, HIGH);
  digitalWrite(red, HIGH);


}

void Temperature(){
  sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);
  Serial.print(temperatureC);
  Serial.println("ºC");
  delay(250);
}
void Photo(){
  int photo_value = analogRead(photo);
  Serial.println(photo_value);
}

void Encoder(){
  // načtení stavu pinu CLK
  stavCLK = digitalRead(pinCLK);
  // pokud je stav CLK odlišný od předchozího měření,
  // víme, že osa byla otočena
  if (stavCLK != stavPred) {
      if (digitalRead(pinDT) != stavCLK) {
        Serial.print("Rotace vpravo => | ");
      poziceEnkod ++;
    }
      else {
        Serial.print("Rotace vlevo  <= | ");
      poziceEnkod--;
    }
    // vytištění aktuální hodnoty pozice enkodéru
    Serial.print("Pozice enkoderu: ");
    Serial.println(poziceEnkod);
  }
  // uložení posledního stavu pinu CLK
    stavPred = stavCLK;
  // načtení stavu pinu SW - tlačítko
  stavSW = digitalRead(pinSW);
   
  if (stavSW == 0) {
    Serial.println("Stisknuto tlacitko enkoderu!");
    delay(500);
  }
}

  void RGB(){
     byte cervena = random(0, 256);
  byte zelena  = random(0, 256);
  byte modra   = random(0, 256);
  byte dioda   = random(1, (pocetLED + 1));
  nastavRGB(cervena, zelena, modra, dioda);
 delay(200);

  }


  // funkce pro nastavení zadané barvy na zvolenou diodu
void nastavRGB (byte r, byte g, byte b, int cislo) {
  // vytvoření proměnné pro ukládání barev
  uint32_t barva;
  // načtení barvy do proměnné
  barva = rgbWS.Color(r, g, b);
  // nastavení barvy pro danou LED diodu,
  // číslo má pořadí od nuly
  rgbWS.setPixelColor(cislo - 1, barva);
  // aktualizace barev na všech modulech
  rgbWS.show();
}
